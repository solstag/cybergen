# Cybergen

"CybergGen is about diaspora women’s voices across digital and off-line spaces"

See the [main project's website](https://cybergen644056625.wordpress.com/) for more details.

This is a small part of a larger study at the intersection of gender, migration, religion and communication, with a focus on the voices of women from diasporas associated with the muslim faith.

We conduct an analysis of public Twitter data on the issue of islamophobia in France and Germany.
