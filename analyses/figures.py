from sashimi.blocks.area_rank_chart import area_rank_chart

from bibliodbs.biblio_twitter import (
    plot_tweet_count,
    user_activity,
    sum_tweet_counts_from_users,
)


def plot_counts(data):
    title = "Islamophobia"
    counts = data.get_counts()
    tweets, users = data.get_tweets_users(augmented=False)
    plot_tweet_count(counts, title=title)
    print(user_activity(tweets, users).head(20))
    print(sum_tweet_counts_from_users(users))


def plot_area_rank_chart(corpus, unit_col="_month"):
    fig = area_rank_chart(
        corpus,
        slice(None),
        f"{unit_col}",
        f"{unit_col}",
        1,
        outfile=f"arc-{unit_col}.html",
    )
    return fig
