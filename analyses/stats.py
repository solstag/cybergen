import pandas as pd

from .. import data


def get_fraction_of_original(corpus):
    size = corpus.data.groupby("_week").size()
    osize = corpus.odata.groupby("_week").size()
    frac = size.div(osize, fill_value=0)
    frac_pct = frac.mul(100).rename("% of corpus")
    return frac_pct


def get_count_authors_with_N_or_more_tweets(corpus_data):
    corpus_data["author_id"].value_counts().value_counts().sort_index(
        ascending=False
    ).cumsum()


def get_domain_users(corpus_data, users, by_cols):
    author_id2username = data.get_id_translator(users, "username")
    domain_users = (
        corpus_data["author_id"]
        .map(lambda x: get_twitter_formatted_link(user=author_id2username(x)))
        .groupby(by_cols)
        .value_counts()
        .rename("count")
    )
    return domain_users


def get_domain_mentions(corpus_data, users, by_cols):
    author_id2username = data.get_id_translator(users, "username")
    domain_mentions = (
        corpus_data["entities"]
        .map(
            lambda x: [
                get_twitter_formatted_link(
                    user=author_id2username(mention["id"], mention["username"])
                )
                for mention in x.get("mentions", [])
            ],
            na_action="ignore",
        )
        .rename("mention")
        .explode()
        .groupby(by_cols)
        .value_counts()
        .rename("count")
    )
    return domain_mentions


def get_domain_conversations(corpus_data, by_cols):
    domain_conversations = (
        corpus_data["conversation_id"]
        .map(lambda x: get_twitter_formatted_link(tweet_id=x))
        .groupby(by_cols)
        .value_counts()
        .rename("count")
    )
    # domain_conversations.to_frame().join(
    #     corpus_data["id"].eq(corpus_data["conversation_id"]).rename("is_top")
    # )
    return domain_conversations


def get_domain_retweet_plus_quote_stats(corpus_data, by_cols):
    domain_retweet_counts = (
        corpus_data["public_metrics"]
        .map(lambda x: x["retweet_count"] + x["quote_count"])
        .groupby(by_cols)
        .describe()
    )
    return domain_retweet_counts


def get_domain_titles(corpus_data, by_cols):
    domain_titles = corpus_data.apply(
        lambda row: (title_parts := data.get_title_parts(row))
        and pd.Series(
            {
                "id": get_twitter_formatted_link(
                    tweet_id=row["id"], user=row["_username"]
                ),
                "user": get_twitter_formatted_link(user=title_parts["user"]),
                "text": title_parts["text"],
                "time": title_parts["time"],
                "rt+qt": title_parts["requote_count"]["rt+qt"],
                "conversation": get_twitter_formatted_link(
                    tweet_id=title_parts["conversation"]
                ),
            }
        ),
        axis=1,
    )
    domain_titles = domain_titles.set_index(by_cols)
    domain_titles = (
        (domain_titles)
        .sort_values("rt+qt", ascending=False, kind="stable")
        .sort_index(kind="stable")
    )
    domain_titles = domain_titles.set_index([domain_titles.index, "id"])
    return domain_titles


####################################
# Make the link formatter settable #
####################################


def dummy_get_twitter_formatted_link(tweet_id=None, user=None):
    return user if tweet_id is None else tweet_id


def set_link_formatter(function):
    global get_twitter_formatted_link
    if function is not None:
        get_twitter_formatted_link = function
    else:
        get_twitter_formatted_link = dummy_get_twitter_formatted_link
