import pandas as pd

from openpyxl.utils import (
    get_column_interval,
    column_index_from_string,
    get_column_letter,
)
from openpyxl.styles import PatternFill

from . import data, sashimi
from . import stats


def store_tables(corpus=None, domain_label=None, by_times=(None, "_week", "_month")):
    if corpus is None:
        corpus = sashimi.load()
        corpus.load_domain_topic_model()
        data.set_sample(corpus)
    _, users = data.get_tweets_users(augmented=False)
    if domain_label:
        corpus_data = corpus.data[corpus.domain_labels_to_selection([domain_label])]
    else:
        corpus_data = corpus.data
    fext = ".xlsx"
    fname_domain = f"-{domain_label}" if domain_label is not None else ""
    fpath = corpus.analysis_dir / f"corpus{fname_domain}{fext}"
    with pd.ExcelWriter(fpath) as writer:
        for by_time in by_times:
            by_cols = get_by_cols(corpus, corpus_data.index, by_time, level=1)
            name_by = (f"_by_{by_time}") if by_time else ""
            to_excel(
                stats.get_domain_titles(corpus_data, by_cols),
                writer,
                "titles" + name_by,
                by_time,
            )
            to_excel(
                stats.get_domain_retweet_plus_quote_stats(corpus_data, by_cols),
                writer,
                "retweet_quote_stats" + name_by,
                by_time,
            )
            to_excel(
                stats.get_domain_mentions(corpus_data, users, by_cols),
                writer,
                "mentions" + name_by,
                by_time,
            )
            to_excel(
                stats.get_domain_users(corpus_data, users, by_cols),
                writer,
                "users" + name_by,
                by_time,
            )
            to_excel(
                stats.get_domain_conversations(corpus_data, by_cols),
                writer,
                "conversations" + name_by,
                by_time,
            )


####################
# Helper functions #
####################


def get_by_cols(corpus, data_index, by_time, level):
    dblocks_label = (
        corpus.dblocks.loc[data_index, level]
        .map(lambda x: corpus.lblock_to_label[level, x])
        .rename("domain")
    )
    by_cols = (
        [dblocks_label, corpus.data.loc[data_index, by_time]]
        if by_time
        else dblocks_label
    )
    return by_cols


def get_xlsx_link(text=None, url=None):
    def quote(x):
        return x.replace('"', '""')

    text, url = quote(text), quote(url)
    return f'=HYPERLINK("{url}", "{text}")'


def get_twitter_xlsx_link(tweet_id=None, user=None):
    if tweet_id is None:
        text = user
        url = data.get_twitter_url(user=user)
    else:
        text = tweet_id
        url = data.get_twitter_url(tweet_id=tweet_id, user=user)
    return get_xlsx_link(text, url)


def to_excel(df, writer, sheet_name, color_on=None):
    df.to_excel(writer, sheet_name, merge_cells=False, freeze_panes=(1, 0))
    sheet = writer.sheets[sheet_name]
    sheet.freeze_panes = "A2"
    max_width = 200 / sheet.max_column
    for col in get_column_interval(1, sheet.max_column):
        col_index = column_index_from_string(col)
        col_length = max(
            len(str(v))
            for c in sheet.iter_cols(col_index, col_index, values_only=True)
            for v in c
        )
        if col_length <= 7:
            sheet.column_dimensions[col].width = min(13, max_width)
        elif col_length <= 17:
            sheet.column_dimensions[col].width = min(23, max_width)
        elif col_length <= 23:
            sheet.column_dimensions[col].width = min(33, max_width)
        else:
            sheet.column_dimensions[col].width = max_width
    if color_on is not None:
        color_on = (
            color_on
            if isinstance(color_on, int) and not isinstance(color_on, bool)
            else next(sheet.iter_rows(values_only=True)).index(color_on)
        )
        start_row = 2
        current_color = "FFFFFF"
        next_color = "EEEEEE"
        current_value = sheet[f"{get_column_letter(color_on + 1)}{start_row}"].value
        for row in sheet.iter_rows(min_row=start_row):
            if row[color_on].value != current_value:
                current_value, current_color, next_color = (
                    row[color_on].value,
                    next_color,
                    current_color,
                )
            for cell in row:
                cell.fill = PatternFill("solid", fgColor=current_color)
    sheet.auto_filter.ref = sheet.dimensions


# Setup the stats module

stats.set_link_formatter(get_twitter_xlsx_link)
