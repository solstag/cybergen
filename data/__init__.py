from datetime import datetime
from html import unescape
import logging
import time
from zoneinfo import ZoneInfo

import pandas as pd

from bibliodbs.biblio_twitter import (
    get_twitter_api_client,
    get_id_translator,
    api_get_counts,
    reload_data_pages,
    api_get_tweets_store_data_pages,
    data_pages_to_dataframes,
    TweetTextCleaner,
)

from . import coding

__all__ = ["coding"]

logger = logging.getLogger(__name__)

VERBOSE = False

TIMEZONE = "Europe/Paris"

START_TIME = datetime(2021, 3, 1, 0, 0, tzinfo=ZoneInfo(TIMEZONE))
START_TIME = (
    START_TIME.astimezone(ZoneInfo("UTC"))
    .replace(tzinfo=None)
    .isoformat(timespec="seconds")
    + "Z"
)
END_TIME = datetime(2022, 6, 1, 0, 0, tzinfo=ZoneInfo(TIMEZONE))
END_TIME = (
    END_TIME.astimezone(ZoneInfo("UTC"))
    .replace(tzinfo=None)
    .isoformat(timespec="seconds")
    + "Z"
)

LANGUAGE = "fr"

TWEETS_FILE_NAME = "tweets_dump.json.zst"


def get_counts(query=None, name=None):
    if query is None:
        name, query = get_name_query()
    client = get_twitter_api_client()
    counts = api_get_counts(client, query, START_TIME, END_TIME, TIMEZONE)
    counts.attrs["name"] = query if name is None else name
    counts.attrs["query"] = query
    return counts


def compare_counts(queries, base_query=None):
    counts = pd.DataFrame()
    try:
        if base_query is None:
            for num, query in enumerate(queries):
                if num:
                    time.sleep(60)
                counts[query] = get_counts(query)["tweet_count"]
        else:
            counts[base_query] = get_counts(base_query)["tweet_count"]
            for query in queries:
                time.sleep(60)
                comp_query = f"{query} -{base_query}"
                counts[comp_query] = get_counts(comp_query)["tweet_count"]
    except Exception as e:
        logger.warning(e)
    counts = counts.fillna(0)
    return counts


def get_tweets_users(augmented=True, raw=False):
    name, query = get_name_query()
    try:
        data_pages = reload_data_pages(TWEETS_FILE_NAME)
    except FileNotFoundError:
        logger.warning("Couldn't find stored data. Downloading a new corpus...")
        client = get_twitter_api_client()
        data_pages = api_get_tweets_store_data_pages(
            client, query, START_TIME, END_TIME, TWEETS_FILE_NAME
        )
    if raw:
        return data_pages
    tweets, users, places = data_pages_to_dataframes(data_pages).values()
    assert tweets["lang"].eq(LANGUAGE).all()
    tweets = tweets.sort_values("created_at")
    if augmented:
        augment_data(tweets, users, places)
    tweets.attrs["name"] = name
    tweets.attrs["query"] = query
    return tweets, users


def get_query_terms():
    terms = [
        "islamophobie",
        # >1k: 2021-04
        "touche pas à mon hijab",
        # "touche pas a mon hijab",
        "touchepasàmonhijab",
        # "touchepasamonhijab",
        # >1k: 2019-07, 2019-09
        "pas vos beurettes",
        "pasvosbeurettes",
        # >1k: 2018-07/08, 2019-01/03, 2019-11, 2020-05, 2021-08/12
        "nta rajel",
        "ntarajel",
        # >1k: 2021-01, 2021-09, 2022-11/12; >10k: 2021-12—2022-10
        "letustalk",
    ]
    quoted_terms = [f'"{x}"' for x in terms]
    return quoted_terms


def get_queries(lang=None, place_country="FR"):
    if lang is None:
        lang = LANGUAGE
    queries = get_query_terms()
    for num, _ in enumerate(queries):
        if lang:
            queries[num] += f" lang:{lang}"
        if place_country:
            queries[num] += f" place_country:{place_country}"
    return queries


def get_name_query():
    """
    https://developer.twitter.com/en/docs/twitter-api/tweets/search/integrate/build-a-query
    """
    name = "islamophobie"
    query_terms = get_query_terms()
    query = " OR ".join(query_terms)
    query = f"({query}) lang:fr"
    query = f"{query} -is:retweet"
    # query = f"{query} -is:nullcast"
    return name, query


def get_conversation(row):
    return row.get("conversation_id", None)


def get_replied_to(row):
    rt = row["referenced_tweets"]
    if isinstance(rt, list):
        return " ".join([x["id"] for x in rt if x["type"] == "replied_to"])
    else:
        return None


def get_requote_count(row):
    pm = row["public_metrics"]
    rc = pm["retweet_count"]
    qc = pm["quote_count"]
    return {"rt+qt": rc + qc, "rt": rc, "qt": qc}


def format_requote_count(rqc):
    return "[rt+qt:{rt}+{qt}={rt+qt}]".format(**rqc)


def get_twitter_url(*, tweet_id=None, user=None):
    if user is None:
        if tweet_id is None:
            raise ValueError("Must provide at least one of tweet_id and user")
        user = "status"
    if tweet_id is None:
        return f"https://twitter.com/{user}"
    return f"https://twitter.com/{user}/status/{tweet_id}"


def get_title_parts(row):
    return {
        "user": row["_username"],
        "text": unescape(row["text"]),
        "time": row["_timestr"],
        "requote_count": get_requote_count(row),
        "conversation": get_conversation(row),
    }


def augment_data(tweets, users, places):
    """
    NOTE: Dot not modify any of the original data so reproducibility can be checked later,
    allowing to further augment the data of an ongoing analysis.
    """
    for x in tweets.columns:
        if x.startswith("_"):
            del tweets[x]

    tweets["_text"] = tweets.agg(TweetTextCleaner.clean_text, axis=1)

    tz_created_at = pd.to_datetime(tweets["created_at"]).dt.tz_convert(TIMEZONE)
    tweets["_timestr"] = tz_created_at.map(lambda x: str(x)[:-9])
    tweets["_day"] = tz_created_at.dt.to_period("D")
    tweets["_week"] = tz_created_at.dt.to_period("W")
    tweets["_month"] = tz_created_at.dt.to_period("M")

    author_id2username = get_id_translator(users, "username")
    tweets["_username"] = tweets["author_id"].map(author_id2username)

    tweets["_url"] = tweets.agg(
        lambda row: get_twitter_url(tweet_id=row["id"], user=row["_username"]), axis=1
    )
    tweets["_title"] = tweets.agg(
        lambda row: (
            "<{user}>: {text} [{time}] {{requote_count}} [c:{conversation}]"
        ).format(
            **{
                **(title_parts := get_title_parts(row)),
                "requote_count": format_requote_count(title_parts["requote_count"]),
            }
        ),
        axis=1,
    )

    place_id2full_name = get_id_translator(places, "full_name")
    tweets["_place"] = tweets.agg(
        lambda x: place_id2full_name(x["geo"]["place_id"])
        if "geo" in x and isinstance(x["geo"], dict)
        else None,
        axis=1,
    )


def set_sample_curated_terms(corpus):
    domain_labels = [
        *coding.get_domains_from_topics_from_terms(
            corpus, coding.get_curated_terms_gender()
        )["counts"]
    ]
    sel = corpus.domain_labels_to_selection(domain_labels)
    corpus.set_sample(sel)


def set_sample_curated_domains(corpus):
    sel = corpus.domain_labels_to_selection(coding.get_curated_domains_gender())
    corpus.set_sample(sel)
