from collections import Counter
from sashimi.blocks.annotations import load_annotations, get_xblock_yblocks_elements


def get_curated_terms_gender():
    return [
        "meuf",
        "meufs",
        "femme",
        "voilée",
        "voilées",
        "corps",
        "foulard",
        "tenue",
        "voiles",
        "émancipation",
        "sœurs",
        "burqa",
        "soumise",
        "mères",
        "baigner",
        "vêtements",
        "féminisme",
        "féministes",
        "soumission",
        "patriarcale",
        "genre",
        "voile",
        "femmes",
        "hijab",
        "touchepasamonhijab",
        "vêtement",
        "vestimentaire",
        "niqab",
        "musulmanes",
        "féministe",
        "sœurs",
        "françaises",
        "femens",
        "femen",
        "sexuelle",
        "sexuelles",
        "filles",
        "letustalk",
        "nohijabday",
        "freefromhijab",
        "burkini",
        "piscine",
        "piscines",
        "fille",
        "oppression",
        "mesdames",
        "beurettes",
        "beurette",
        "pasvosbeurettes",
        "rajel",
        "nta",
        "sexisme",
        "badinter",
        "zineb",
        "rhazoui",
        "diams",
        "misogyne",
        "lgbt",
        "pastoucheamonhijab",
        "lallab",
        "féminicides",
    ]


def get_curated_terms_veil():
    return [
        "voilée",
        "voilées",
        "foulard",
        "tenue",
        "burqa",
        "baigner",
        "vêtements",
        "vêtement",
        "voiles",
        "voile",
        "vestimentaire",
        "hijab",
        "niqab",
        "touchepasamonhijab",
        "letustalk",
        "nohijabday",
        "freefromhijab",
        "burkini",
        "piscine",
        "piscines",
        "pastoucheamonhijab",
    ]


def get_curated_domains_gender():
    """
    Domains curated by Nina and Ale
    """
    return [
        "L1D291",
        "L1D288",
        "L1D282",
        "L1D254",
        "L1D252",
        "L1D251",
        "L1D243",
        "L1D241",
        "L1D237",
        "L1D224",
        "L1D223",
        "L1D122",
    ]


class OldModel:
    topics_veil = [
        "L1T367",
        "L1T363",
        "L1T304",
        "L1T365",
        "L1T362",
        "L1T354",
        "L1T361",
        "L1T355",
        "L1T352",
        "L1T70",
        "L1T319",
        "L1T0",
    ]

    topics_to_exclude = ["L1T369"]


topics_to_exclude = ["L1T346"]


def get_topics_terms(corpus, terms):
    topics_terms = {}
    for term in terms:
        hb = tuple(reversed(tuple(corpus.tblocks.loc[term, corpus.tblocks_levels])))
        l1b = corpus.hblock_to_label[hb]
        if l1b not in topics_to_exclude:
            topics_terms.setdefault(l1b, []).append(term)
    return topics_terms


def get_topics_domains(corpus, selected_topics):
    dlevel = 1
    dlevel_annotations = load_annotations(
        corpus, get_xblock_yblocks_elements, "doc", "ter", ylevel=1, num=None
    )[dlevel]
    topic2domains = {}
    for db in dlevel_annotations:
        db_label = corpus.lblock_to_label[dlevel, db]
        topics = dlevel_annotations[db]["blocks"]
        for tb in topics:
            tb_label = corpus.lblock_to_label[1, tb]
            if tb_label in selected_topics:
                topic2domains.setdefault(tb_label, []).append(db_label)
    return topic2domains


def get_domains_from_topics_from_terms(corpus, terms):
    topics2terms = get_topics_terms(corpus, terms)
    topics2domains = get_topics_domains(corpus, topics2terms)
    domains2topics = {}
    for x, y in topics2domains.items():
        for ye in y:
            domains2topics.setdefault(ye, []).append(x)
    domains_topics_count = Counter((x for y in topics2domains.values() for x in y))
    return dict(
        topics2terms=topics2terms,
        topics2domains=topics2domains,
        domains2topics=domains2topics,
        counts=domains_topics_count,
    )
