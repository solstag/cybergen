import time
from functools import wraps
from pathlib import Path
import logging as logging
import pandas as pd

from sashimi.graph_models.domain_topic_model import refine_state

from . import data

logger = logging.getLogger(__name__)

try:
    from sashimi import GraphModels
except ImportError:
    logger.warning("Sashimi not available.")


def load_gender(config_path=None):
    corpus = load(config_path)
    corpus.load_domain_topic_model()
    data.set_sample_curated_domains(corpus)
    return corpus


def load(config_path=None):
    config_path = config_path or Path("sashimi_config.json")
    tweets, _ = data.get_tweets_users()
    if config_path.exists():
        corpus = GraphModels(config_path)
        attrs = corpus.data.attrs.copy()
        corpus.data = pd.concat([tweets, corpus.data], axis=1, verify_integrity=True)
        corpus.data.attrs.update(attrs)
        corpus.odata = corpus.data
        return corpus
    else:
        return bootstrap(tweets, config_path)


def bootstrap(data, config_path):
    config_path = Path(config_path)
    if config_path.exists():
        raise FileExistsError(f"File '{config_path}' already exists")

    corpus = GraphModels(
        config_path=config_path,
        text_sources=["_text"],
        col_id="id",
        col_title="_title",
        col_time="_day",
        col_url="_url",
        col_venue="_place",
    )

    corpus.load_data(data, data.attrs["name"])
    corpus.process_sources(language="fr", stop_words=True)
    corpus.store_data(corpus.column)
    corpus.register_config()
    loaded_corpus = load(config_path=config_path)
    if not corpus.data.equals(loaded_corpus.data):
        raise RuntimeError("Reloaded corpus differs from original")
    return loaded_corpus


def timed(fun):
    @wraps(fun)
    def wrapper(*args, **kwargs):
        print(time.localtime())
        res = fun(*args, **kwargs)
        print(time.localtime())
        return res

    return wrapper


@timed
def new_model(chained="_week"):
    corpus = load()
    corpus.load_domain_topic_model(load=chained)
    if chained:
        data.set_sample_curated_domains(corpus)
        corpus.set_chain(chained)
        corpus.load_domain_chained_model(load=False)
    return corpus


@timed
def refine_model(strategy):
    corpus = load()
    corpus.load_blockstate()
    refine_state(corpus.state, strategy)
    return corpus
